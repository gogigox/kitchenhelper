## **Description**

This app provides three food-related functionalities. First one is wine pairing - user is searching for wine that goes well with food(that he writes in). Recommended wines will be displayed and also description below. Second is find substitutes - it helps to find substitutes for cooking ingredients(e.g., user can find ingredients that can replace butter). Third is recipes - there are meal categories(e.g., lamb, chicken, dessert...) that provide list of recipes for that category. User can also see recipe detail which contains instructions and ingredients needed.

In order to have cleaner and more extensible code, architecture of this app is built in MVVM design pattern. All live data are supplied from internet, using REST API and connection with two websites: https://spoonacular.com/ and https://www.themealdb.com ( Retrofit2 is used to consume REST API). For thumbnail images Glide library is used.

Guided by the principles of responsive UI design this app is customized for different screen sizes and resolutions.




## **Screenshots**

![screenshot1](https://gitlab.com/gogigox/kitchenhelper/-/blob/master/screenshots/Screenshot_20200913-094943.png)
<!--  -->
![screenshot2](https://gitlab.com/gogigox/kitchenhelper/-/blob/master/screenshots/Screenshot_20200913-095027.png)
<!--  -->
![screenshot3](https://gitlab.com/gogigox/kitchenhelper/-/blob/master/screenshots/Screenshot_20200913-095052.png)
<!--  -->
![screenshot4](https://gitlab.com/gogigox/kitchenhelper/-/blob/master/screenshots/Screenshot_20200913-095245.png)
<!--  -->
![screenshot5](https://gitlab.com/gogigox/kitchenhelper/-/blob/master/screenshots/Screenshot_20200913-095653.png)
<!--  -->
![screenshot6](https://gitlab.com/gogigox/kitchenhelper/-/blob/master/screenshots/Screenshot_20200913-100919.png)
