package com.example.kitchenhelper;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.kitchenhelper.views.FindSubstitutesActivity;
import com.example.kitchenhelper.views.CategoryListActivity;
import com.example.kitchenhelper.views.WinePairingActivity;


public class MainActivity extends AppCompatActivity {

    private CardView cvWinePairing, cvSubstitutes, cvRecipes;
    private TextView tvWinePairing, tvSubstitutes, tvRecipes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cvWinePairing = findViewById(R.id.card_layout_wine);
        cvSubstitutes = findViewById(R.id.card_layout_substitutes);
        cvRecipes = findViewById(R.id.card_layout_recipes);

        tvWinePairing = findViewById(R.id.tv_wine_pairing);
        tvSubstitutes = findViewById(R.id.tv_substitutes);
        tvRecipes = findViewById(R.id.tv_recipes);


        cvWinePairing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getBaseContext(), WinePairingActivity.class);
                startActivity(intent);
            }
        });

        cvSubstitutes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getBaseContext(), FindSubstitutesActivity.class);
                startActivity(intent);
            }
        });

        cvRecipes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getBaseContext(), CategoryListActivity.class);
                startActivity(intent);
            }
        });


    }
}