package com.example.kitchenhelper.model.findsubstitutes;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class Substitute {

    @SerializedName("ingredient")
    @Expose
    private String ingredient;
    @SerializedName("substitutes")
    @Expose
    private List<String> substitutes = null;
    @SerializedName("message")
    @Expose
    private String message;

    public String getIngredient() {
        return ingredient;
    }

    public void setIngredient(String ingredient) {
        this.ingredient = ingredient;
    }

    public List<String> getSubstitutes() {
        return substitutes;
    }

    public void setSubstitutes(List<String> substitutes) {
        this.substitutes = substitutes;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
