package com.example.kitchenhelper.model.winepairing;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductMatch {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("averageRating")
    @Expose
    private Double averageRating;
    @SerializedName("description")
    @Expose
    private Object description;
    @SerializedName("imageUrl")
    @Expose
    private String imageUrl;
    @SerializedName("link")
    @Expose
    private String link;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("ratingCount")
    @Expose
    private Double ratingCount;
    @SerializedName("score")
    @Expose
    private Double score;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Double getAverageRating() {
        return averageRating;
    }

    public void setAverageRating(Double averageRating) {
        this.averageRating = averageRating;
    }

    public Object getDescription() {
        return description;
    }

    public void setDescription(Object description) {
        this.description = description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public Double getRatingCount() {
        return ratingCount;
    }

    public void setRatingCount(Double ratingCount) {
        this.ratingCount = ratingCount;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

}
