package com.example.kitchenhelper.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.kitchenhelper.R;
import com.example.kitchenhelper.model.recipes.Meals;

import java.util.List;


public class RecipeListAdapter extends RecyclerView.Adapter<RecipeListAdapter.MyViewHolder> {


    private List<Meals.Meal> results;

    public RecipeListAdapter.OnRVItemClick listenerRecipeList;

    public interface OnRVItemClick {
        void onRVItemclick(Meals.Meal meal);
    }

    public RecipeListAdapter(List<Meals.Meal> results, OnRVItemClick listenerRecipeList) {
        this.results = results;
        this.listenerRecipeList = listenerRecipeList;
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView ivMealThumb;
        private TextView tvMealName;
        View view;

        public MyViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            ivMealThumb = itemView.findViewById(R.id.iv_meal_thumb);
            tvMealName = itemView.findViewById(R.id.tv_meal_name);



        }

        public void bind(final Meals.Meal meal, final RecipeListAdapter.OnRVItemClick listener) {

            if (meal != null) {
                String imageUrl = meal.getStrMealThumb()
                        .replace("http://", "https://");

                Glide.with(itemView)
                        .load(imageUrl)
                        .into(ivMealThumb);

                tvMealName.setText(meal.getStrMeal());

                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listener.onRVItemclick(meal);
                    }
                });

            }
        }
    }

    @NonNull
    @Override
    public RecipeListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater
                .from(viewGroup.getContext())
                .inflate(R.layout.recipe_list_single_item, viewGroup, false);
        return new MyViewHolder(view);
    }


    @Override
    public void onBindViewHolder(RecipeListAdapter.MyViewHolder myViewHolder, int i) {

        myViewHolder.bind(results.get(i), listenerRecipeList);
    }


    @Override
    public int getItemCount() {
        return results.size();
    }


    public void setResults(List<Meals.Meal> results) {
        this.results = results;
        notifyDataSetChanged();
    }


}
