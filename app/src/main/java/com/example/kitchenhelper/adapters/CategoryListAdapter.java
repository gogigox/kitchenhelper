package com.example.kitchenhelper.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.kitchenhelper.R;
import com.example.kitchenhelper.model.categorylist.Categories;

import java.util.List;


public class CategoryListAdapter extends RecyclerView.Adapter<CategoryListAdapter.MyViewHolder> {


    private List<Categories.Category> results;

    public CategoryListAdapter.OnRVItemClick listenerCategoryList;

    public interface OnRVItemClick {
        void onRVItemclick(Categories.Category category);

    }

    public CategoryListAdapter(List<Categories.Category> results, OnRVItemClick listenerCategoryList) {
        this.results = results;
        this.listenerCategoryList = listenerCategoryList;
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView ivMealCategory;
        private EditText etCategoryName;
        View view;

        public MyViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            ivMealCategory = itemView.findViewById(R.id.iv_meal_category);
            etCategoryName = itemView.findViewById(R.id.et_category_name);
            etCategoryName.setFocusable(false);
        }

        public void bind(final Categories.Category category, final CategoryListAdapter.OnRVItemClick listener) {


            if (category != null) {
                String imageUrl = category.getStrCategoryThumb()
                        .replace("http://", "https://");

                Glide.with(itemView)
                        .load(imageUrl)
                        .into(ivMealCategory);


                etCategoryName.setText(category.getStrCategory());

                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listener.onRVItemclick(category);
                    }
                });

            }
        }
    }

    @NonNull
    @Override
    public CategoryListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater
                .from(viewGroup.getContext())
                .inflate(R.layout.category_list_single_item, viewGroup, false);
        return new MyViewHolder(view);
    }


    @Override
    public void onBindViewHolder(CategoryListAdapter.MyViewHolder myViewHolder, int i) {

        myViewHolder.bind(results.get(i), listenerCategoryList);
    }


    @Override
    public int getItemCount() {
        return results.size();
    }


    public void setResults(List<Categories.Category> results) {
        this.results = results;
        notifyDataSetChanged();
    }


}
