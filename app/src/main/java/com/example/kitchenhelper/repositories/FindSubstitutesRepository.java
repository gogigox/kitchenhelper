package com.example.kitchenhelper.repositories;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.kitchenhelper.api.FindSubstitutesService;
import com.example.kitchenhelper.model.findsubstitutes.Substitute;

import org.jetbrains.annotations.NotNull;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.converter.gson.GsonConverterFactory;



public class FindSubstitutesRepository {

    private static final String BASE_URL = "https://api.spoonacular.com/";
    private static final String KEY = "db21d5a12f7541cdacefb83a14014b1a";

    private FindSubstitutesService findSubstitutesService;
    private MutableLiveData<Substitute> substituteLiveData;


    public FindSubstitutesRepository() {
        substituteLiveData = new MutableLiveData<>();

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.level(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        findSubstitutesService = new retrofit2.Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(FindSubstitutesService.class);
    }

    public void findSubstitute(String ingredientName) {
        findSubstitutesService.findSubstitute(KEY, ingredientName)
                .enqueue(new Callback<Substitute>() {
                    @Override
                    public void onResponse(@NotNull Call<Substitute> call, @NotNull Response<Substitute> response) {
                        if (response.body() != null) {
                            substituteLiveData.postValue(response.body());
                        }
                    }

                    @Override
                    public void onFailure(@NotNull Call<Substitute> call, Throwable t) {
                        substituteLiveData.postValue(null);
                    }
                });
    }

    public LiveData<Substitute> getSubstitutesLiveData() {
        return substituteLiveData;
    }





}
