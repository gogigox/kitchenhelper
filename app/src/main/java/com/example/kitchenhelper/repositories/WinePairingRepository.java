package com.example.kitchenhelper.repositories;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.kitchenhelper.api.WinePairingService;
import com.example.kitchenhelper.model.winepairing.Wine;

import org.jetbrains.annotations.NotNull;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.converter.gson.GsonConverterFactory;


public class WinePairingRepository {

    private static final String BASE_URL = "https://api.spoonacular.com/";
    private static final String KEY = "db21d5a12f7541cdacefb83a14014b1a";

    private WinePairingService winePairingService;
    private MutableLiveData<Wine> wineLiveData;

    public WinePairingRepository() {
        wineLiveData = new MutableLiveData<>();

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.level(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        winePairingService = new retrofit2.Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(WinePairingService.class);
    }

    public void searchWine(String food) {
        winePairingService.searchWine(KEY, food)
                .enqueue(new Callback<Wine>() {
                    @Override
                    public void onResponse(@NotNull Call<Wine> call, @NotNull Response<Wine> response) {
                        if (response.body() != null) {
                            wineLiveData.postValue(response.body());
                        }
                    }

                    @Override
                    public void onFailure(@NotNull Call<Wine> call, Throwable t) {
                        wineLiveData.postValue(null);
                    }
                });
    }

    public LiveData<Wine> getWineLiveData() {
        return wineLiveData;
    }


}
