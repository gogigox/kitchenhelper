package com.example.kitchenhelper.repositories;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.kitchenhelper.api.CategoryListService;
import com.example.kitchenhelper.model.categorylist.Categories;

import org.jetbrains.annotations.NotNull;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.converter.gson.GsonConverterFactory;


public class CategoryListRepository {

    private static final String BASE_URL = "https://www.themealdb.com/api/json/v1/1/";

    private CategoryListService categoryListService;
    private MutableLiveData<Categories> categoriesLiveData;


    public CategoryListRepository() {
        categoriesLiveData = new MutableLiveData<>();

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.level(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        categoryListService = new retrofit2.Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(CategoryListService.class);
    }


    public void searchCategories() {
        categoryListService.getCategories()
                .enqueue(new Callback<Categories>() {
                    @Override
                    public void onResponse(@NotNull Call<Categories> call, @NotNull Response<Categories> response) {
                        if (response.body() != null) {
                            categoriesLiveData.postValue(response.body());
                        }
                    }

                    @Override
                    public void onFailure(@NotNull Call<Categories> call, Throwable t) {
                        categoriesLiveData.postValue(null);
                    }
                });
    }


    public LiveData<Categories> getCategLiveData() {

        return categoriesLiveData;
    }

}
