package com.example.kitchenhelper.repositories;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.example.kitchenhelper.api.RecipeService;
import com.example.kitchenhelper.model.recipes.Meals;

import org.jetbrains.annotations.NotNull;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.converter.gson.GsonConverterFactory;


public class RecipeListRepository {

    private static final String BASE_URL = "https://www.themealdb.com/api/json/v1/1/";

    private RecipeService recipeService;
    private MutableLiveData<Meals> mealLiveData;


    public RecipeListRepository() {
        mealLiveData = new MutableLiveData<>();

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.level(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        recipeService = new retrofit2.Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(RecipeService.class);
    }

    public void searchRecipes(String category) {
        recipeService.searchRecipeList(category)
                .enqueue(new Callback<Meals>() {
                    @Override
                    public void onResponse(@NotNull Call<Meals> call, @NotNull Response<Meals> response) {
                        if (response.body() != null) {
                            mealLiveData.postValue(response.body());
                        }
                    }

                    @Override
                    public void onFailure(@NotNull Call<Meals> call, Throwable t) {
                        mealLiveData.postValue(null);
                    }
                });
    }

    public LiveData<Meals> getRecipeLiveData() {
        return mealLiveData;
    }


}
