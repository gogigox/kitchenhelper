package com.example.kitchenhelper.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.kitchenhelper.R;
import com.example.kitchenhelper.model.recipes.Meals;
import com.example.kitchenhelper.viewmodels.RecipeDetailViewModel;


public class RecipeDetailActivity extends AppCompatActivity {

    private ImageView ivMealThumb;
    private EditText etMealName, etInstruction, etIngredients, etMeasures;
    private RecipeDetailViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe_detail);

        ivMealThumb = findViewById(R.id.iv_meal_thumb);
        etMealName = findViewById(R.id.et_meal_name);
        etMealName.setFocusable(false);
        etInstruction = findViewById(R.id.et_instruction);
        etInstruction.setFocusable(false);
        etIngredients = findViewById(R.id.et_ingredients);
        etIngredients.setFocusable(false);
        etMeasures = findViewById(R.id.et_measures);
        etMeasures.setFocusable(false);

        viewModel = new ViewModelProvider(this).get(RecipeDetailViewModel.class);
        viewModel.init();
        viewModel.getMealsLiveData().observe(this, new Observer<Meals>() {
            @Override
            public void onChanged(Meals meals) {
                if (meals != null) {

                    String imageUrl = meals.getMeals().get(0).getStrMealThumb()
                            .replace("http://", "https://");

                    Glide.with(getApplicationContext())
                            .load(imageUrl)
                            .into(ivMealThumb);


                    etMealName.setText(meals.getMeals().get(0).getStrMeal());
                    etInstruction.setText(meals.getMeals().get(0).getStrInstructions());

                    //pregledan prikaz sastojaka i njihovih kolicina(od 119.reda)
                    if (meals.getMeals().get(0).getStrIngredient1() != null && !meals.getMeals().get(0).getStrIngredient1().isEmpty()) {
                        etIngredients.append("\n \u2022 " + meals.getMeals().get(0).getStrIngredient1());
                    }
                    if (meals.getMeals().get(0).getStrIngredient2() != null && !meals.getMeals().get(0).getStrIngredient2().isEmpty()) {
                        etIngredients.append("\n \u2022 " + meals.getMeals().get(0).getStrIngredient2());
                    }
                    if (meals.getMeals().get(0).getStrIngredient3() != null && !meals.getMeals().get(0).getStrIngredient3().isEmpty()) {
                        etIngredients.append("\n \u2022 " + meals.getMeals().get(0).getStrIngredient3());
                    }
                    if (meals.getMeals().get(0).getStrIngredient4() != null && !meals.getMeals().get(0).getStrIngredient4().isEmpty()) {
                        etIngredients.append("\n \u2022 " + meals.getMeals().get(0).getStrIngredient4());
                    }
                    if (meals.getMeals().get(0).getStrIngredient5() != null && !meals.getMeals().get(0).getStrIngredient5().isEmpty()) {
                        etIngredients.append("\n \u2022 " + meals.getMeals().get(0).getStrIngredient5());
                    }
                    if (meals.getMeals().get(0).getStrIngredient6() != null && !meals.getMeals().get(0).getStrIngredient6().isEmpty()) {
                        etIngredients.append("\n \u2022 " + meals.getMeals().get(0).getStrIngredient6());
                    }
                    if (meals.getMeals().get(0).getStrIngredient7() != null && !meals.getMeals().get(0).getStrIngredient7().isEmpty()) {
                        etIngredients.append("\n \u2022 " + meals.getMeals().get(0).getStrIngredient7());
                    }
                    if (meals.getMeals().get(0).getStrIngredient8() != null && !meals.getMeals().get(0).getStrIngredient8().isEmpty()) {
                        etIngredients.append("\n \u2022 " + meals.getMeals().get(0).getStrIngredient8());
                    }
                    if (meals.getMeals().get(0).getStrIngredient9() != null && !meals.getMeals().get(0).getStrIngredient9().isEmpty()) {
                        etIngredients.append("\n \u2022 " + meals.getMeals().get(0).getStrIngredient9());
                    }
                    if (meals.getMeals().get(0).getStrIngredient10() != null && !meals.getMeals().get(0).getStrIngredient10().isEmpty()) {
                        etIngredients.append("\n \u2022 " + meals.getMeals().get(0).getStrIngredient10());
                    }
                    if (meals.getMeals().get(0).getStrIngredient11() != null && !meals.getMeals().get(0).getStrIngredient11().isEmpty()) {
                        etIngredients.append("\n \u2022 " + meals.getMeals().get(0).getStrIngredient11());
                    }
                    if (meals.getMeals().get(0).getStrIngredient12() != null && !meals.getMeals().get(0).getStrIngredient12().isEmpty()) {
                        etIngredients.append("\n \u2022 " + meals.getMeals().get(0).getStrIngredient12());
                    }
                    if (meals.getMeals().get(0).getStrIngredient13() != null && !meals.getMeals().get(0).getStrIngredient13().isEmpty()) {
                        etIngredients.append("\n \u2022 " + meals.getMeals().get(0).getStrIngredient13());
                    }
                    if (meals.getMeals().get(0).getStrIngredient14() != null && !meals.getMeals().get(0).getStrIngredient14().isEmpty()) {
                        etIngredients.append("\n \u2022 " + meals.getMeals().get(0).getStrIngredient14());
                    }
                    if (meals.getMeals().get(0).getStrIngredient15() != null && !meals.getMeals().get(0).getStrIngredient15().isEmpty()) {
                        etIngredients.append("\n \u2022 " + meals.getMeals().get(0).getStrIngredient15());
                    }
                    if (meals.getMeals().get(0).getStrIngredient16() != null && !meals.getMeals().get(0).getStrIngredient16().isEmpty()) {
                        etIngredients.append("\n \u2022 " + meals.getMeals().get(0).getStrIngredient16());
                    }
                    if (meals.getMeals().get(0).getStrIngredient17() != null && !meals.getMeals().get(0).getStrIngredient17().isEmpty()) {
                        etIngredients.append("\n \u2022 " + meals.getMeals().get(0).getStrIngredient17());
                    }
                    if (meals.getMeals().get(0).getStrIngredient18() != null && !meals.getMeals().get(0).getStrIngredient18().isEmpty()) {
                        etIngredients.append("\n \u2022 " + meals.getMeals().get(0).getStrIngredient18());
                    }
                    if (meals.getMeals().get(0).getStrIngredient19() != null && !meals.getMeals().get(0).getStrIngredient19().isEmpty()) {
                        etIngredients.append("\n \u2022 " + meals.getMeals().get(0).getStrIngredient19());
                    }
                    if (meals.getMeals().get(0).getStrIngredient20() != null && !meals.getMeals().get(0).getStrIngredient20().isEmpty()) {
                        etIngredients.append("\n \u2022 " + meals.getMeals().get(0).getStrIngredient20());
                    }


                    if (meals.getMeals().get(0).getStrMeasure1() != null && !meals.getMeals().get(0).getStrMeasure1().isEmpty()) {
                        etMeasures.append("\n : " + meals.getMeals().get(0).getStrMeasure1());
                    }
                    if (meals.getMeals().get(0).getStrMeasure2() != null && !meals.getMeals().get(0).getStrMeasure2().isEmpty()) {
                        etMeasures.append("\n : " + meals.getMeals().get(0).getStrMeasure2());
                    }
                    if (meals.getMeals().get(0).getStrMeasure3() != null && !meals.getMeals().get(0).getStrMeasure3().isEmpty()) {
                        etMeasures.append("\n : " + meals.getMeals().get(0).getStrMeasure3());
                    }
                    if (meals.getMeals().get(0).getStrMeasure4() != null && !meals.getMeals().get(0).getStrMeasure4().isEmpty()) {
                        etMeasures.append("\n : " + meals.getMeals().get(0).getStrMeasure4());
                    }
                    if (meals.getMeals().get(0).getStrMeasure5() != null && !meals.getMeals().get(0).getStrMeasure5().isEmpty()) {
                        etMeasures.append("\n : " + meals.getMeals().get(0).getStrMeasure5());
                    }
                    if (meals.getMeals().get(0).getStrMeasure6() != null && !meals.getMeals().get(0).getStrMeasure6().isEmpty()) {
                        etMeasures.append("\n : " + meals.getMeals().get(0).getStrMeasure6());
                    }
                    if (meals.getMeals().get(0).getStrMeasure7() != null && !meals.getMeals().get(0).getStrMeasure7().isEmpty()) {
                        etMeasures.append("\n : " + meals.getMeals().get(0).getStrMeasure7());
                    }
                    if (meals.getMeals().get(0).getStrMeasure8() != null && !meals.getMeals().get(0).getStrMeasure8().isEmpty()) {
                        etMeasures.append("\n : " + meals.getMeals().get(0).getStrMeasure8());
                    }
                    if (meals.getMeals().get(0).getStrMeasure9() != null && !meals.getMeals().get(0).getStrMeasure9().isEmpty()) {
                        etMeasures.append("\n : " + meals.getMeals().get(0).getStrMeasure9());
                    }
                    if (meals.getMeals().get(0).getStrMeasure10() != null && !meals.getMeals().get(0).getStrMeasure10().isEmpty()) {
                        etMeasures.append("\n : " + meals.getMeals().get(0).getStrMeasure10());
                    }
                    if (meals.getMeals().get(0).getStrMeasure11() != null && !meals.getMeals().get(0).getStrMeasure11().isEmpty()) {
                        etMeasures.append("\n : " + meals.getMeals().get(0).getStrMeasure11());
                    }
                    if (meals.getMeals().get(0).getStrMeasure12() != null && !meals.getMeals().get(0).getStrMeasure12().isEmpty()) {
                        etMeasures.append("\n : " + meals.getMeals().get(0).getStrMeasure12());
                    }
                    if (meals.getMeals().get(0).getStrMeasure13() != null && !meals.getMeals().get(0).getStrMeasure13().isEmpty()) {
                        etMeasures.append("\n : " + meals.getMeals().get(0).getStrMeasure13());
                    }
                    if (meals.getMeals().get(0).getStrMeasure14() != null && !meals.getMeals().get(0).getStrMeasure14().isEmpty()) {
                        etMeasures.append("\n : " + meals.getMeals().get(0).getStrMeasure14());
                    }
                    if (meals.getMeals().get(0).getStrMeasure15() != null && !meals.getMeals().get(0).getStrMeasure15().isEmpty()) {
                        etMeasures.append("\n : " + meals.getMeals().get(0).getStrMeasure15());
                    }
                    if (meals.getMeals().get(0).getStrMeasure16() != null && !meals.getMeals().get(0).getStrMeasure16().isEmpty()) {
                        etMeasures.append("\n : " + meals.getMeals().get(0).getStrMeasure16());
                    }
                    if (meals.getMeals().get(0).getStrMeasure17() != null && !meals.getMeals().get(0).getStrMeasure17().isEmpty()) {
                        etMeasures.append("\n : " + meals.getMeals().get(0).getStrMeasure17());
                    }
                    if (meals.getMeals().get(0).getStrMeasure18() != null && !meals.getMeals().get(0).getStrMeasure18().isEmpty()) {
                        etMeasures.append("\n : " + meals.getMeals().get(0).getStrMeasure18());
                    }
                    if (meals.getMeals().get(0).getStrMeasure19() != null && !meals.getMeals().get(0).getStrMeasure19().isEmpty()) {
                        etMeasures.append("\n : " + meals.getMeals().get(0).getStrMeasure19());
                    }
                    if (meals.getMeals().get(0).getStrMeasure20() != null && !meals.getMeals().get(0).getStrMeasure20().isEmpty()) {
                        etMeasures.append("\n : " + meals.getMeals().get(0).getStrMeasure20());
                    }

                }
            }
        });

        performSearch();

    }


    public void performSearch() {
        String mealName = getIntent().getStringExtra("mealName");
        viewModel.searchRecipeDetail(mealName);
    }


}