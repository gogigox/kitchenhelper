package com.example.kitchenhelper.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;

import com.example.kitchenhelper.R;
import com.example.kitchenhelper.adapters.CategoryListAdapter;
import com.example.kitchenhelper.model.categorylist.Categories;
import com.example.kitchenhelper.viewmodels.CategoryListViewModel;

import java.util.ArrayList;
import java.util.List;


public class CategoryListActivity extends AppCompatActivity implements CategoryListAdapter.OnRVItemClick {

    private CategoryListViewModel viewModel;
    private CategoryListAdapter adapter;
    private List<Categories.Category> categoryList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_list);


        adapter = new CategoryListAdapter(categoryList, this);

        viewModel = new ViewModelProvider(this).get(CategoryListViewModel.class);
        viewModel.init();
        viewModel.getCategoriesLiveData().observe(this, new Observer<Categories>() {
            @Override
            public void onChanged(Categories categories) {
                if (categories != null) {
                    adapter.setResults(categories.getCategories());
                }
            }
        });

        RecyclerView recyclerView = findViewById(R.id.rv_list);
        GridLayoutManager layoutManager = new GridLayoutManager(this, 2,
                GridLayoutManager.VERTICAL, false);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);


        performSearch();

    }


    public void performSearch() {
        viewModel.searchCategories();
    }


    @Override
    public void onRVItemclick(Categories.Category category) {

        Intent intent = new Intent(this, RecipeListActivity.class);
        intent.putExtra("category", category.getStrCategory());
        startActivity(intent);

    }

}