package com.example.kitchenhelper.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;

import com.example.kitchenhelper.R;
import com.example.kitchenhelper.adapters.RecipeListAdapter;
import com.example.kitchenhelper.model.recipes.Meals;
import com.example.kitchenhelper.viewmodels.RecipeListViewModel;

import java.util.ArrayList;
import java.util.List;


public class RecipeListActivity extends AppCompatActivity implements RecipeListAdapter.OnRVItemClick {


    private RecipeListViewModel viewModel;
    private RecipeListAdapter adapter;

    private List<Meals.Meal> mealList = new ArrayList<>();
    private List<Meals.Meal> recipeList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe_list);

        setUpRecipeList();


        adapter = new RecipeListAdapter(recipeList, this);

        viewModel = new ViewModelProvider(this).get(RecipeListViewModel.class);
        viewModel.init();
        viewModel.getMealsLiveData().observe(this, new Observer<Meals>() {
            @Override
            public void onChanged(Meals meals) {
                if (meals != null) {
                    adapter.setResults(meals.getMeals());
                }
            }
        });

        RecyclerView recyclerView = findViewById(R.id.rv_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setAdapter(adapter);

        performSearch();

    }


    public void setUpRecipeList() {

        String strCategory = getIntent().getStringExtra("category");

        for (Meals.Meal m : mealList) {
            if (m.getStrCategory().equals(strCategory)) {
                recipeList.add(m);
            }
        }
    }


    public void performSearch() {
        String strCategory = getIntent().getStringExtra("category");
        viewModel.searchRecipes(strCategory);
    }


    @Override
    public void onRVItemclick(Meals.Meal meal) {

        Intent intent = new Intent(this, RecipeDetailActivity.class);
        intent.putExtra("mealName", meal.getStrMeal());
        startActivity(intent);

    }
}