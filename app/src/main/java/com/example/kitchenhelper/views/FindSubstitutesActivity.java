package com.example.kitchenhelper.views;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.example.kitchenhelper.R;
import com.example.kitchenhelper.Util;
import com.example.kitchenhelper.model.findsubstitutes.Substitute;
import com.example.kitchenhelper.viewmodels.FindSubstitutesViewModel;


public class FindSubstitutesActivity extends AppCompatActivity {

    private TextView tvTitle, tvIngredient, tvMessage, tvSubstitutes;
    private EditText etIngredientName, etMessageText, etResult;
    private Button btnSearch;
    private FindSubstitutesViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_substitutes);

        tvTitle = findViewById(R.id.tv_title);
        tvIngredient = findViewById(R.id.tv_ingredient);
        tvMessage = findViewById(R.id.tv_message);
        tvSubstitutes = findViewById(R.id.tv_substitutes);
        etIngredientName = findViewById(R.id.et_ingredient_name);
        etMessageText = findViewById(R.id.et_message_text);
        etMessageText.setFocusable(false);
        etResult = findViewById(R.id.et_result);
        etResult.setFocusable(false);
        btnSearch = findViewById(R.id.button_search);

        viewModel = new ViewModelProvider(this).get(FindSubstitutesViewModel.class);
        viewModel.init();
        viewModel.getSubstituteLiveData().observe(this, new Observer<Substitute>() {
            @Override
            public void onChanged(Substitute substitute) {
                if (substitute != null) {

                    etMessageText.setText(substitute.getMessage());

                    if (substitute.getSubstitutes() != null) {
                        Util u = new Util();
                        String substitutes = u.StringJoin(substitute.getSubstitutes(), "," + "\n");
                        etResult.setText(substitutes);
                    } else {
                        etResult.setText("");
                    }
                }
            }
        });

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                performSearch();

            }
        });

    }


    public void performSearch() {

        String ingredient = etIngredientName.getText().toString();
        viewModel.findSubstitute(ingredient);
    }
}