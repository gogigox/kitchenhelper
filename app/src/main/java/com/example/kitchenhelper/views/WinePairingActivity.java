package com.example.kitchenhelper.views;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.Observer;

import com.example.kitchenhelper.R;
import com.example.kitchenhelper.Util;
import com.example.kitchenhelper.model.winepairing.Wine;
import com.example.kitchenhelper.viewmodels.WinePairingViewModel;


public class WinePairingActivity extends AppCompatActivity {

    private TextView tvTitle, tvFood, tvRecomWines, tvDescription;
    private EditText etFoodName, etResult, etDescResult;
    private Button btnSearch;
    private WinePairingViewModel viewModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wine_pairing);

        tvTitle = findViewById(R.id.tv_title);
        tvFood = findViewById(R.id.tv_food);
        tvRecomWines = findViewById(R.id.tv_recom_wines);
        tvDescription = findViewById(R.id.tv_description);
        etFoodName = findViewById(R.id.et_food_name);
        etResult = findViewById(R.id.et_result);
        etResult.setFocusable(false);
        etDescResult = findViewById(R.id.et_desc_result);
        etDescResult.setFocusable(false);
        btnSearch = findViewById(R.id.button_search);


        viewModel = new ViewModelProvider(this).get(WinePairingViewModel.class);
        viewModel.init();
        viewModel.getWineLiveData().observe(this, new Observer<Wine>() {
            @Override
            public void onChanged(Wine wine) {
                if (wine != null) {
                    if (wine.getPairedWines() != null) {
                        Util u = new Util();
                        String wines = u.StringJoin(wine.getPairedWines(), ", ");
                        etResult.setText(wines);
                    }

                    etDescResult.setText(wine.getPairingText());

                }
            }
        });


        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                performSearch();

            }
        });
    }


    public void performSearch() {

        String food = etFoodName.getText().toString();
        viewModel.searchWine(food);

    }

}
