package com.example.kitchenhelper.api;

import com.example.kitchenhelper.model.categorylist.Categories;

import retrofit2.Call;
import retrofit2.http.GET;


public interface CategoryListService {

    //podatke citamo sa sajta https://www.themealdb.com/
    @GET("categories.php")
    Call<Categories> getCategories();
}
