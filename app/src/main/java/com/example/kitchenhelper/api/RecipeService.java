package com.example.kitchenhelper.api;

import com.example.kitchenhelper.model.recipes.Meals;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;


public interface RecipeService {

    //podatke citamo sa sajta https://www.themealdb.com/

    @GET("filter.php")
    Call<Meals> searchRecipeList(
            @Query("c") String category);


    @GET("search.php")
    Call<Meals> searchRecipeDetail(
            @Query("s") String mealName);
}
