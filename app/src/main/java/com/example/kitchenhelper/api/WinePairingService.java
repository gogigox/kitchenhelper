package com.example.kitchenhelper.api;

import com.example.kitchenhelper.model.winepairing.Wine;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface WinePairingService {

    //podatke citamo sa sajta https://api.spoonacular.com/
    @GET("/food/wine/pairing")
    Call<Wine> searchWine(
            @Query("apiKey") String apiKey,
            @Query ("food") String food
    );
}
