package com.example.kitchenhelper.api;



import com.example.kitchenhelper.model.findsubstitutes.Substitute;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface FindSubstitutesService {

    //podatke citamo sa sajta https://api.spoonacular.com/
    @GET("/food/ingredients/substitutes")
    Call<Substitute> findSubstitute(
            @Query("apiKey") String apiKey,
            @Query ("ingredientName") String ingredientName
    );
}
