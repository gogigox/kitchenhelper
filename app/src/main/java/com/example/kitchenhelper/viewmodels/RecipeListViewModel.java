package com.example.kitchenhelper.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.kitchenhelper.model.recipes.Meals;
import com.example.kitchenhelper.repositories.RecipeListRepository;


public class RecipeListViewModel extends AndroidViewModel {


    private RecipeListRepository recipeListRepository;
    private LiveData<Meals> mealsLiveData;


    public RecipeListViewModel(@NonNull Application application) {
        super(application);
    }


    public void init() {
        recipeListRepository = new RecipeListRepository();
        mealsLiveData = recipeListRepository.getRecipeLiveData();
    }


    public void searchRecipes(String category) {
        recipeListRepository.searchRecipes(category);
    }

    public LiveData<Meals> getMealsLiveData() {
        return mealsLiveData;
    }


}
