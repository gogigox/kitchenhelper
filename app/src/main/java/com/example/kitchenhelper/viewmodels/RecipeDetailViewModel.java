package com.example.kitchenhelper.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.kitchenhelper.model.recipes.Meals;
import com.example.kitchenhelper.repositories.RecipeDetailRepository;


public class RecipeDetailViewModel extends AndroidViewModel {


    private RecipeDetailRepository recipeDetailRepository;
    private LiveData<Meals> mealsLiveData;


    public RecipeDetailViewModel(@NonNull Application application) {
        super(application);
    }


    public void init() {
        recipeDetailRepository = new RecipeDetailRepository();
        mealsLiveData = recipeDetailRepository.getRecipeDetailLiveData();
    }


    public void searchRecipeDetail(String mealName) {
        recipeDetailRepository.searchRecipeDeatail(mealName);
    }

    public LiveData<Meals> getMealsLiveData() {
        return mealsLiveData;
    }

}
