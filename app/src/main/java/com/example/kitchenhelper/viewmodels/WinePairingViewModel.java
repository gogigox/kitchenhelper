package com.example.kitchenhelper.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.kitchenhelper.model.winepairing.Wine;
import com.example.kitchenhelper.repositories.WinePairingRepository;


public class WinePairingViewModel extends AndroidViewModel {

    private WinePairingRepository winePairingRepository;
    private LiveData<Wine> wineLiveData;


    public WinePairingViewModel(@NonNull Application application) {
        super(application);
    }


    public void init() {
        winePairingRepository = new WinePairingRepository();
        wineLiveData = winePairingRepository.getWineLiveData();
    }


    public void searchWine(String food) {
        winePairingRepository.searchWine(food);
    }


    public LiveData<Wine> getWineLiveData() {

        return wineLiveData;
    }


}
