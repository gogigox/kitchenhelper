package com.example.kitchenhelper.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.kitchenhelper.model.categorylist.Categories;
import com.example.kitchenhelper.repositories.CategoryListRepository;


public class CategoryListViewModel extends AndroidViewModel {


    private CategoryListRepository categoryListRepository;
    private LiveData<Categories> categoriesLiveData;


    public CategoryListViewModel(@NonNull Application application) {
        super(application);
    }

    public void init() {
        categoryListRepository = new CategoryListRepository();
        categoriesLiveData = categoryListRepository.getCategLiveData();
    }



    public void searchCategories()  {
        categoryListRepository.searchCategories();
    }

    public LiveData<Categories> getCategoriesLiveData() {

        return categoriesLiveData;
    }

}
