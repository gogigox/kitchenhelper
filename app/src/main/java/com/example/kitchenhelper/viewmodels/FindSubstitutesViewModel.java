package com.example.kitchenhelper.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.kitchenhelper.model.findsubstitutes.Substitute;
import com.example.kitchenhelper.repositories.FindSubstitutesRepository;



public class FindSubstitutesViewModel extends AndroidViewModel {


    private FindSubstitutesRepository findSubstitutesRepository;
    private LiveData<Substitute> substituteLiveData;


    public FindSubstitutesViewModel(@NonNull Application application) {
        super(application);
    }



    public void init() {
        findSubstitutesRepository = new FindSubstitutesRepository();
        substituteLiveData = findSubstitutesRepository.getSubstitutesLiveData();
    }


    public void findSubstitute(String ingredientName) {
        findSubstitutesRepository.findSubstitute(ingredientName);
    }


    public LiveData<Substitute> getSubstituteLiveData() {

        return substituteLiveData;
    }

}
